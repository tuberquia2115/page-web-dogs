import produce from 'immer';

import { FETCH_GET_ALL_SUBBREEDS, PUT_GET_ALL_SUBBREEDS } from '../actions/types';

const initialState = {
  subBreeds: [],
  isFetchingLoading: false,
};

export default (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case FETCH_GET_ALL_SUBBREEDS:
        draft.isFetchingLoading = true;
        break;

      case PUT_GET_ALL_SUBBREEDS:
        draft.subBreeds = action.subBreeds;
        draft.isFetchingLoading = false;
        break;
      default:
        break;
    }
  });
