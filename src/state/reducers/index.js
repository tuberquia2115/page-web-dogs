import { combineReducers } from 'redux';

import subBreed from './subBreedReducer';

const rootReducer = combineReducers({
    subBreed,
  });
export default rootReducer;
