import { FETCH_GET_ALL_SUBBREEDS, PUT_GET_ALL_SUBBREEDS } from './types';

export const fetchGetAllSubBreeds = (breed) => ({
  type: FETCH_GET_ALL_SUBBREEDS,
  breed
});

export const putGetAllSubBreeds = (subBreeds) => ({
  type: PUT_GET_ALL_SUBBREEDS,
  subBreeds,
});

export default {
  fetchGetAllSubBreeds,
  putGetAllSubBreeds
}