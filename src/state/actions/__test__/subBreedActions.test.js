import subBreedActions from '../subBreedActions';


describe('subBreed actions', () => {
  const actionArray = Object.keys(subBreedActions);
  describe.each(actionArray)('GIVEN %s action', (action) => {
    describe('WHEN the action is called', () => {
      const result = subBreedActions[action]();

      it('THEN shounld be an object', () => {
        expect(typeof result).toBe('object');
      });
    });

  });
});
