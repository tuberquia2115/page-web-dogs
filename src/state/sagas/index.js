import { all } from 'redux-saga/effects';

import getAllSubBreeds from './subBreeds';

export default function* rootSaga() {
  yield all([getAllSubBreeds()]);
}
