import { call, put, takeLatest } from 'redux-saga/effects';

import { getListAllSubBreeds } from '../../../services/api/subBreed';
import { putGetAllSubBreeds } from '../../actions/subBreedActions';
import { FETCH_GET_ALL_SUBBREEDS } from '../../actions/types';

export function* fetchGetAllSubBreeds({ breed }) {
  try {
    const subBreeds = yield call(getListAllSubBreeds, breed);
    console.log('subBreeds', subBreeds);
    yield put(putGetAllSubBreeds(subBreeds));
  } catch (error) {
    yield put(putGetAllSubBreeds([]));
  }
}

export default function* watchFetchGetAllSubBreeds() {
  yield takeLatest(FETCH_GET_ALL_SUBBREEDS, fetchGetAllSubBreeds);
}
