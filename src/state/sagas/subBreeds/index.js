import { all } from 'redux-saga/effects';

import getAllSubBreedsSaga from './getAllSubBreedsSaga';

export default function* rootSaga() {
  yield all([getAllSubBreedsSaga()]);
}
