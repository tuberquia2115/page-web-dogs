import React from 'react';
import Modal from 'react-bootstrap/Modal';
import ModalHeader from 'react-bootstrap/ModalHeader';
import ModalBody from 'react-bootstrap/ModalBody';
import ModalFooter from 'react-bootstrap/ModalFooter';
import Button from 'react-bootstrap/Button';

export const ModalGeneric = ({ show, onToggle }) => {
  return (
    <Modal show={show} onHide={onToggle} centered={true} backdrop="static" keyboard={false}>
      <ModalHeader>Soy un modal</ModalHeader>
      <ModalBody>Dios los bendiga</ModalBody>
      <ModalFooter>
        <Button variant="secondary" onClick={onToggle}>
          Close
        </Button>
      </ModalFooter>
    </Modal>
  );
};
