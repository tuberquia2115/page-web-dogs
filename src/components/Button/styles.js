import styled from '@emotion/styled';

export const CustomButton = styled('button')`
  background-color: ${(props) => (props.backColor ? props.backColor : '#f4f4')};
  padding: '10px';
  width: '10%';
`;
