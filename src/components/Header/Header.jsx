import React from 'react';

import { Image } from '../Image';
import { Navbar, Container, ContainerLogo, NavList, NavItem, StyledNavLink } from './styles';

const links = [
  { to: '/', name: 'Home' },
  { to: '/dashboard', name: 'Dogs' },
];
const Header = ({ src, title }) => {
  return (
    <Navbar>
      <ContainerLogo>
        <Image width={20} src={src} />{' '}
        <strong>
          <em>{title}</em>
        </strong>
      </ContainerLogo>
      <Container>
        <NavList>
          {links.map(({ to, name }) => (
            <NavItem>
              <StyledNavLink to={to} exact activeClassName="active">
                {name}
              </StyledNavLink>
            </NavItem>
          ))}
        </NavList>
      </Container>
    </Navbar>
  );
};
export default Header;
