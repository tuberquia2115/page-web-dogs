import styled from '@emotion/styled';
import { NavLink } from 'react-router-dom';

import { displayFlex } from '../../styles';

export const Navbar = styled.nav({
  ...displayFlex,
  justifyContent: 'space-between',
  backgroundColor: '#282c34',
  padding: '10px 20px',
});

export const ContainerLogo = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  justify-content: start;

  & > strong {
    color: var(--white);
    font-size: 1.5em;
  }
`;

export const Container = styled('div')``;

export const NavList = styled.ul`
  list-style: none;
  display: flex;
  margin: 0px;
  padding: 0px;
`;

export const StyledNavLink = styled(NavLink)`
  text-decoration: none;
  color: var(--white);
  font-weight: 600;
  font-style: italic;
`;
export const NavItem = styled.li`
  text-decoration: none;
  margin: 20px;
  & .active {
    background-color: var(--white);
    padding: 10px;
    border-radius: 10px;
    color: #000000;
  }
`;
