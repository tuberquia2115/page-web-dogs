import React from 'react';
import PropTypes from 'prop-types';

import defaultSrc from '../../assets/img/image-default.png';

import CustomImage from './styles';

export const Image = ({ width, src, fullWidth, alt }) => {
  return <CustomImage src={src} width={Number(width)} alt={alt} fullWidth={fullWidth} />;
};

Image.propTypes = {
  width: PropTypes.number,
  src: PropTypes.string,
  fullWidth: PropTypes.bool,
  alt: PropTypes.string,
};

Image.defaultProps = {
  width: 100,
  src: defaultSrc,
  fullWidth: false,
  alt: 'Img default',
};
