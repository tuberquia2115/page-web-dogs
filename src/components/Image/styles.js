import styled from '@emotion/styled';

const Image = styled.img`
  max-width: ${(props) => props.fullWidth ? `${100}%` : `${props.width}%`};
  width: 100%;
  height: auto;
`;



export default Image;
