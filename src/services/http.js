import axios from 'axios';

const http = axios.create({
  baseURL: 'https://dog.ceo/api',
  withCredentials: false,
  headers: { Accept: "application/json", "Content-Type": "application/json" },
});

http.interceptors.response.use(
  async (config) => {
    return config;
  },
  (error) => {
    const alertHidden = ['400', '401', '404'].includes(error.response.status);
    alertHidden && alert('Error', `MESSAGE: ${error.message}\n\nSERVICE: ${error.config.url}`);
    return Promise.reject(error.response ? error.response.data : error.response);
  }
);

export default http;
