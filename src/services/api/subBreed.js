import { http } from '..';

export const getListAllSubBreeds = async (breed) => {
  try {
    const {
      data: { message },
    } = await http.get(`/breed/${breed}/list`);

    return message;
  } catch (error) {
    throw error;
  }
};

export const getListAllSubBreedImages = async (breed, subBreed) => {
  try {
    const { message } = await http.get(`/breed/${breed}/${subBreed}/images`);
    return message;
  } catch (error) {
    throw error;
  }
};

export const getImageSubBreedRandom = async (breed, subBreed) => {
  try {
    const data = await http.get(`/breed/${breed}/${subBreed}/images/random`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const getMultipleImagesBySubBreed = async (breed, subBreed, number) => {
  try {
    const data = await http.get(`/breed/${breed}/${subBreed}/images/random/${number}`);
    return data;
  } catch (error) {
    throw error;
  }
};
