import { http } from '../';

export const getListAllBreeds = async () => {
  try {
    const data = await http.get(`/breeds/list/all`);
    console.log("respuesta ",response);
    return data;
  } catch (error) {
    throw error;
  }
};

export const getAllImagesByBreed = async (breed) => {
  try {
    const data = await http.get(`breed/${breed}/images`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const getImageRandomByBreed = async (breed) => {
  try {
    const data = await http.get(`breed/${breed}/images/random`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const getMultipleImagesByBreed = async (breed, number) => {
  try {
    const data = await http.get(`breed/${breed}/images/random/${number}`);
    return data;
  } catch (error) {
    throw error;
  }
};
