import { http } from '..';

export const getImageRandomFromAllDogs = async () => {
  try {
    const data = await http.get(`breeds/image/random`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const getMultipleRandomImagesFromAllDogs = async (number) => {
  try {
    const data = await http.get(`breeds/image/random/${number}`);
    return data;
  } catch (error) {
    throw error;
  }
};
