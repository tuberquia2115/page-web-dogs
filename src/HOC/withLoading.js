import React from 'react';

// cambiar este HOC
const withloading = (breed) => {
  return (WrappedComponet) => {
    const hoc = ({ isFetchingLoading, getAllListSubBreed, ...props }) => {
      React.useEffect(() => {
        getAllListSubBreed();
      }, []);
      if (isFetchingLoading) {
        return <p>Loading...</p>;
      }
      const subBreeds = [...props.subBreeds, breed];

      return <WrappedComponet {...props} subBreeds={subBreeds} />;
    };
    hoc.displayName = `withloading(${WrappedComponet.displayName || WrappedComponet.name})`;
    return hoc;
  };
};

export default withloading;
