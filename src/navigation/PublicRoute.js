import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

export const PublicRoute = ({ isLoggedIn, component: Component, ...rest }) => {
  return <Route {...rest} component={(props) => (!isLoggedIn ? <Component {...props} /> : <Redirect to="/" />)} />;
};

PublicRoute.propTypes = {
  isLoggedIn: PropTypes.bool,
  component: PropTypes.func,
};
