import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';

export const PrivateRoute = ({ isLoggedIn, component: Component, ...rest }) => {
  return (
    <Route {...rest} component={(props) => (isLoggedIn ? <Component {...props} /> : <Redirect to="/" />)} />
  );
};

PrivateRoute.propTypes = {
  isLoggedIn: PropTypes.bool,
  component: PropTypes.func,
};
