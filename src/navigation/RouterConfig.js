import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from '../components/Header';

import DashboardPage from '../pages/Dashboard';
import HomePage from '../pages/Home';
import { AppRoutes } from './AppRoutes';
import NotFound from './NotFound';
import logoSrc from '../assets/img/logo.png'

const RouterConfig = () => (
  <Router>
    <Header src={logoSrc} title="DOGS" />
    <Switch>
      <Route exact path={AppRoutes.DASHBOARD} children={<DashboardPage />} />
      <Route exact path={AppRoutes.HOME} children={<HomePage />} />
      <Route path="*" children={<NotFound />} />
    </Switch>
  </Router>
);

export default RouterConfig;
