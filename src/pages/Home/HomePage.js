import React from 'react';


import withloading from '../../HOC/withLoading';

//cambiar
const HomePage = ({ subBreeds }) => {
  const CardItem = ({ breed }) => {
    return (
      <div style={{ width: '18rem' }} >
        <h4 color="">Razas</h4>
        <div>
          <p color="">{breed}</p>
        </div>
      </div>
    );
  };
  return (
    <div>
      <h2>SUB RAZAS</h2>
      <div>
        {subBreeds.map((breed, index) => (
          <CardItem breed={breed} key={index} />
        ))}
      </div>
    </div>
  );
};

export default withloading("chandorman")(HomePage);
