import { StoreService } from '../../state/StoreService';
import { fetchGetAllSubBreeds } from '../../state/actions/subBreedActions';
import HomePage from './HomePage';


 export const mapStateToProps = (state) => ({
  subBreeds: state.subBreed.subBreeds,
  isFetchingLoading: state.subBreed.isFetchingLoading
});

export const mapDispatchToProps = (dispatch) => ({
  getAllListSubBreed: () => {
    dispatch(fetchGetAllSubBreeds('hound'));
  },
});

const HomePageContainer = StoreService.connect(mapStateToProps, mapDispatchToProps)(HomePage);

export default HomePageContainer;
