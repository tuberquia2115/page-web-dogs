import DashboardPage from './DashboardPage';
import { fetchGetAllSubBreeds } from '../../state/actions/subBreedActions';
import { StoreService } from '../../state/StoreService.js';

export const mapStateToProps = (state) => ({
  subBreed: state.subBreed.subBreeds,
});

export const mapDispatchToProps = (dispatch) => {
  return {
    getAllListSubreed: (breed = 'hound') => {
      dispatch(fetchGetAllSubBreeds(breed));
    },
  };
};

const DashboardPageContainer = StoreService.connect(mapStateToProps, mapDispatchToProps)(DashboardPage);

export default DashboardPageContainer;
