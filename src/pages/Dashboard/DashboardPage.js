import React, { useLayoutEffect } from 'react';

const DashboardPage = ({ subBreed, getAllListSubreed }) => {
  useLayoutEffect(() => {
    getAllListSubreed();
  }, []);
  return (
    <div>
      <h1 >Dashboard </h1>
    </div>
  );
};

export default DashboardPage;
